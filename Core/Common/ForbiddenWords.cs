﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core
{
    public static class ForbiddenWords
    {
        // 금칙어 명단
        private static Dictionary<string/*forbidde, word*/, string/*filtered word*/> _ForbiddneWords;


        static ForbiddenWords()
        {
            _ForbiddneWords = new Dictionary<string, string> ();

            #region 금칙어와 관련된 내용을 초기화합니다.
            _ForbiddneWords.Add("뻐큐", "엿먹어!");
            #endregion
        }

        /// <summary>
        /// 금칙어 포함 여부를 확인합니다.
        /// </summary>
        /// <param name="checkString">검사할 문자열을 전달합니다.</param>
        /// <param name="forbiddenWords">확인된 금칙어들을 저장할 리스틀르 전달합니다.</param>
        /// <param name=""></param>
        /// <returns></returns>
        public static bool Exist(string checkString, out List<string> forbiddenWords)
        {
            forbiddenWords = new List<string> ();

            // 금칙어가 포함되어있는지 확인합니다.
            foreach(string forbiddenWord in _ForbiddneWords.Keys)
            {
                // 금칙어가 포함되어있다면 리스트에 추가
                if(checkString.Contains(forbiddenWord))
                    forbiddenWords.Add(forbiddenWord);
            }

            // 금칙어 포함 여부
            return forbiddenWords.Count != 0;
        }
    }
}
