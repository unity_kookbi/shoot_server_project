using ClientSample;
using System.Windows.Forms;

ApplicationConfiguration.Initialize();
FirstPage firstPage = new FirstPage();
Application.Run(firstPage);

await firstPage.ConnectAsync();

namespace ClientSample
{


	internal static class Program
	{
		/// <summary>
		///  The main entry point for the application.
		/// </summary>
		[STAThread]
		private static async Task Main2()
		{
			// To customize application configuration such as set high DPI settings or default font,
			// see https://aka.ms/applicationconfiguration.
			
		}
	}
}