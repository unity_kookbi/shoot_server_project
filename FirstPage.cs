using System.Net;
using System.Net.Sockets;
using System.Windows.Forms;

namespace ClientSample
{
	public partial class FirstPage : Form
	{
		private Socket _ConnectedServerSocket;

		public FirstPage()
		{
			InitializeComponent();

			//Label_Result.Text = "Hello World!";
		}

		public async Task ConnectAsync()
		{
			_ConnectedServerSocket = new(
				AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
			IPEndPoint endPoint = new(
				IPAddress.Parse(Core.Constants.ADDRESS),
				Core.Constants.SERVERPORT);

			// 2초 대기
			await Task.Delay(2000);

			// 연결시킵니다.
			await _ConnectedServerSocket.ConnectAsync(endPoint);

			Label_Result.Text = "연결되었습니다!";
		}
	}
}
